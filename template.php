<?php

/**
 * Override default region style.
 */
function precision_panels_default_style_render_region($display, $region_id, $panes, $settings) {
  $output = '';
  foreach ($panes as $pane_id => $pane_output) {
    $output .= $pane_output;
  }
  return $output;
}

/**
 * Generic function that modifies some variables in all Precision layouts.
 */
function precision_check_layout_variables(&$vars) {
  $vars['css_id'] = strtr($vars['css_id'], '_', '-');
}
