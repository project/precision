<?php

$plugin = array(
  'title' => t('Precision naked'),
  'theme' => 'precision_naked',
  'icon' => 'precision-naked.png',
  'panels' => array(
    'main' => t('Main'),
  ),
);

/**
 * Preprocess the naked layout.
 */
function precision_preprocess_precision_naked(&$vars) {
  precision_check_layout_variables($vars);
}
